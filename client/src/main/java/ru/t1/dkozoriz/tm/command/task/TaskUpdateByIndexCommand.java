package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    public TaskUpdateByIndexCommand() {
        super("task-update-by-index", "update task by index.");
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        getEndpointLocator().getTaskEndpoint()
                .taskUpdateByIndex(new TaskUpdateByIndexRequest(getToken(), index, name, description));
    }

}