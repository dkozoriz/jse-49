package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectRemoveByIndexRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    public ProjectRemoveByIndexCommand() {
        super("project-remove-by-index", "remove project by index.");
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        getEndpointLocator().getProjectEndpoint()
                .projectRemoveByIndex(new ProjectRemoveByIndexRequest(getToken(), index));
    }

}