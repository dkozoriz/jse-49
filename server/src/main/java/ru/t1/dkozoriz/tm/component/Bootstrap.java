package ru.t1.dkozoriz.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.endpoint.*;
import ru.t1.dkozoriz.tm.api.service.*;
import ru.t1.dkozoriz.tm.api.service.dto.ISessionDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.ITaskDtoService;
import ru.t1.dkozoriz.tm.api.service.model.business.IProjectService;
import ru.t1.dkozoriz.tm.api.service.model.ISessionService;
import ru.t1.dkozoriz.tm.api.service.model.business.ITaskService;
import ru.t1.dkozoriz.tm.api.service.model.IUserService;
import ru.t1.dkozoriz.tm.endpoint.*;
import ru.t1.dkozoriz.tm.service.*;
import ru.t1.dkozoriz.tm.service.dto.SessionDtoService;
import ru.t1.dkozoriz.tm.service.dto.business.ProjectDtoService;
import ru.t1.dkozoriz.tm.service.dto.business.TaskDtoService;
import ru.t1.dkozoriz.tm.service.dto.UserDtoService;
import ru.t1.dkozoriz.tm.service.model.SessionService;
import ru.t1.dkozoriz.tm.service.model.UserService;
import ru.t1.dkozoriz.tm.service.model.business.ProjectService;
import ru.t1.dkozoriz.tm.service.model.business.TaskService;
import ru.t1.dkozoriz.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ITaskDtoService taskDtoService = new TaskDtoService(this);

    @Getter
    @NotNull
    private final IProjectDtoService projectDtoService = new ProjectDtoService(this);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(this);

    @Getter
    @NotNull
    private final IUserDtoService userDtoService = new UserDtoService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(this);

    @Getter
    @NotNull
    private final ISessionDtoService sessionDtoService = new SessionDtoService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }


    private void prepareStartup() {
        loggerService.info("*** TASK MANAGER SERVER IS STARTED ***");
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
    }

    public void run(@Nullable final String... args) {
        prepareStartup();
    }

}