package ru.t1.dkozoriz.tm.api.service.dto.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.dkozoriz.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.dkozoriz.tm.dto.model.business.BusinessModelDto;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;

import java.util.List;

public interface IBusinessDtoService<T extends BusinessModelDto> extends IUserOwnedDtoService<T> {

    @NotNull
    T changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    T changeStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    @Nullable
    List<T> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    T updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    T updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}