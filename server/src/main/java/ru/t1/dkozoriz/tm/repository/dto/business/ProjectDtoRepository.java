package ru.t1.dkozoriz.tm.repository.dto.business;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.dto.business.IProjectDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;

import javax.persistence.EntityManager;

public final class ProjectDtoRepository extends BusinessDtoRepository<ProjectDto> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<ProjectDto> getClazz() {
        return ProjectDto.class;
    }

}