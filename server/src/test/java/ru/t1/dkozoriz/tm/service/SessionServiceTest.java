package ru.t1.dkozoriz.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.dto.ISessionDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.component.Bootstrap;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.marker.UnitCategory;
import ru.t1.dkozoriz.tm.migration.AbstractSchemeTest;
import ru.t1.dkozoriz.tm.service.dto.SessionDtoService;
import ru.t1.dkozoriz.tm.service.dto.UserDtoService;

import java.util.ArrayList;
import java.util.List;


@Category(UnitCategory.class)
public class SessionServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final IServiceLocator SERVICE_LOCATOR = new Bootstrap();

    @NotNull
    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String userId1 = "";

    @NotNull
    private static String userId2 = "";

    @NotNull
    private final List<SessionDto> sessionList = new ArrayList<>();

    @NotNull
    private static final ISessionDtoService sessionService = new SessionDtoService(SERVICE_LOCATOR);

    @NotNull
    private static final IUserDtoService userService = new UserDtoService(SERVICE_LOCATOR);

    @BeforeClass
    public static void beforeTest() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        userId1 = userService.create("user1", "pass", Role.USUAL).getId();
        userId2 = userService.create("user2", "pass", Role.USUAL).getId();
    }

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final SessionDto session = new SessionDto();
            if (i <= 10 / 2) {
                session.setUserId(userId1);
                session.setRole(userService.findById(userId1).getRole());
            }
            else {
                session.setUserId(userId2);
                session.setRole(userService.findById(userId2).getRole());
            }
            sessionService.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAddSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final SessionDto session = new SessionDto();
        session.setUserId(userId1);
        session.setRole(userService.findById(userId1).getRole());
        sessionService.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testAddUserOwnedSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final SessionDto session = new SessionDto();
        session.setUserId(userId1);
        session.setRole(userService.findById(userId1).getRole());
        sessionService.add(userId1, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testUpdateUserOwnedSession() {
        final int expectedNumberOfEntries = 0;
        sessionService.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testClearSession() {
        final long expectedNumberOfEntries = 0;
        sessionService.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testClearUserOwnedSession() {
        final long expectedNumberOfEntries = sessionService.getSize(userId2);
        sessionService.clear(userId1);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testFindAllSession() {
        final List<SessionDto> findAllSessionList = sessionService.findAll();
        Assert.assertEquals(findAllSessionList.size(), sessionService.getSize());
    }

    @Test
    public void testFindAllUserOwnedSession() {
        final List<SessionDto> findAllSessionList = sessionService.findAll(userId1);
        Assert.assertEquals(findAllSessionList.size(), sessionService.getSize(userId1));
    }

    @Test
    public void testFindByIdSession() {
        final String sessionId = sessionList.get(0).getId();
        final SessionDto session = sessionService.findById(sessionId);
        Assert.assertNotNull(session);
    }

    @Test
    public void testFindByIdUserOwnedSession() {
        final String sessionId = sessionList.get(0).getId();
        final SessionDto session = sessionService.findById(userId1, sessionId);
        Assert.assertNotNull(session);
    }

    @Test
    public void testRemoveSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String sessionId = sessionList.get(0).getId();
        final SessionDto session = sessionService.findById(sessionId);
        sessionService.remove(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testRemoveUserOwnedSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String sessionId = sessionList.get(0).getId();
        final SessionDto session = sessionService.findById(userId1, sessionId);
        sessionService.remove(userId1, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testRemoveByIdSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String sessionId = sessionList.get(0).getId();
        sessionService.removeById(sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testRemoveByIdUserOwnedSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String sessionId = sessionList.get(0).getId();
        sessionService.removeById(userId1, sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testGetSizeSession() {
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
    }

    @Test
    public void testGetSizeUserOwnedSession() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES / 2;
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(userId1));
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(userId2));
    }

    @After
    public void clearData() {
        sessionList.clear();
        sessionService.clear();
    }

}