package ru.t1.dkozoriz.tm.dto.model.business;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.enumerated.Status;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDto extends BusinessModelDto implements IWBS {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public TaskDto(@NotNull final String name) {
        super(name);
    }

    public TaskDto(@NotNull final String name, @NotNull final Status status) {
        super(name, status);
    }

    public TaskDto(@NotNull final String name, @NotNull final String description) {
        super(name, description);
    }

}